cc=gcc
pd_src=play_database/src/
pd_hdr=play_database/hdr/
dp_src=data_processing/src/
dp_hdr=data_processing/hdr/
obj=bin/
flags=-Wall ${FLAGS}
lib=-lpthread -lplay_database -ldata_process
lDir=lib/
dir=-I$(pd_hdr) -I$(dp_hdr) -L$(lDir)

all : gipsy

run : gipsy
	./gipsy

debug : gipsy
	gdb gipsy

leak-check : gipsy
	valgrind --leak-check=full --track-origins=yes --show-leak-kinds=all ./gipsy

gipsy : directory main.c $(lDir)libplay_database.a $(lDir)libdata_process.a
	$(cc) $(flags) main.c -o $@ $(dir) $(lib)

$(lDir)libdata_process.a : $(obj)parameter.o $(obj)state_of_form.o $(obj)param_array.o $(obj)data_processing.o $(obj)main_ai.o $(obj)select_neuron.o
	ar -r $@ $^

$(obj)select_neuron.o : $(dp_src)select_neuron.c
	$(cc) $(flags) $^ -c -o $@ $(dir)

$(obj)param_array.o : $(dp_src)param_array.c
	$(cc) $(flags) $^ -c -o $@ $(dir)

$(obj)data_processing.o : $(dp_src)data_processing.c
	$(cc) $(flags) $^ -c -o $@ $(dir)

$(obj)main_ai.o : $(dp_src)main_ai.c
	$(cc) $(flags) $^ -c -o $@ $(dir)

$(obj)parameter.o : $(dp_src)parameter.c
	$(cc) $(flags) $^ -c -o $@ $(dir)

$(obj)state_of_form.o : $(dp_src)state_of_form.c
	$(cc) $(flags) $^ -c -o $@ $(dir)

$(lDir)libplay_database.a : $(obj)play_database.o $(obj)date.o $(obj)json_manip.o $(obj)league.o $(obj)list_play.o $(obj)play.o $(obj)list_team.o $(obj)team.o $(obj)odd.o $(obj)result.o $(obj)thread.o
	ar -r $@ $^

$(obj)thread.o : $(pd_src)thread.c
	$(cc) $(flags) $^ -c -o $@ $(lib)

$(obj)play_database.o : $(pd_src)play_database.c
	$(cc) $(flags) $^ -c -o $@ $(lib)

$(obj)date.o : $(pd_src)date.c
	$(cc) $(flags) $^ -c -o $@

$(obj)json_manip.o : $(pd_src)json_manip.c
	$(cc) $(flags) $^ -c -o $@

$(obj)league.o : $(pd_src)league.c
	$(cc) $(flags) $^ -c -o $@

$(obj)list_play.o : $(pd_src)list_play.c
	$(cc) $(flags) $^ -c -o $@

$(obj)play.o : $(pd_src)play.c
	$(cc) $(flags) $^ -c -o $@

$(obj)list_team.o : $(pd_src)list_team.c
	$(cc) $(flags) $^ -c -o $@

$(obj)team.o : $(pd_src)team.c
	$(cc) $(flags) $^ -c -o $@

$(obj)odd.o : $(pd_src)odd.c
	$(cc) $(flags) $^ -c -o $@

$(obj)result.o : $(pd_src)result.c
	$(cc) $(flags) $^ -c -o $@

directory :
	mkdir -p lib
	mkdir -p bin

clean :
	rm bin/*
	rm $(lDir)*
	rm gipsy
#ifndef __THREAD_H__
#define __THREAD_H__ 0

#include <pthread.h>
#include <semaphore.h>

/**
 * Construct and build plays from the json_play passed in arguments
 * Function made to be run as a thread routine
 */
void* construct_and_build_play(void* arg);

/**
 * Get a list of plays in argument, create a thread for each of them that will
 * construct a play, build it, and insert it in the plays list
 * params : char * containing a valid list of json plays
 * Function made to be run as a thread routine
 */
void * construct_and_build(void * arg);

#endif
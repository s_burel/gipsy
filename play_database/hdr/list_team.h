#ifndef __LIST_TEAM_H__
#define __LIST_TEAM_H__ 0

#include "team.h"

/**
 * Struct describing a node of a team list
 */
struct l_t_node {
  struct l_t_node * next;
  struct l_t_node * prev;
  struct team * t;
};

/**
 * Main list of the teams
 * This list include all the teams
 */
struct l_t_node * list_team_main;
pthread_mutex_t list_team_mutex;

/* Init the list_team_main */
void init_l_t();

/* Destruct the list of team without touching the teams */
void destruct_list_team_light(struct l_t_node * node);

/* Destruct all the team of the list */
void destruct_all_teams_of_the_list(struct l_t_node * node);

/* Destruct the list of team and all the teams */
void destruct_list_team(struct l_t_node * node);

/**
 * Build and insert a team in list
 * params : struct l_t_node * to the node where to insert the team
 *          char * to the name of the team
 */
void insert_team_in_list(struct l_t_node * node, char * name);

/**
 * Get team from char
 * If the team does not exist, return created one
 * params : char * to the name of the team
 * return : struct league * to the team
 */
struct team * get_team_from_string(char * string);

/* Order the play list for each team in the list */
void order_play_list_for_each_team_of_list(struct l_t_node * node);

#endif
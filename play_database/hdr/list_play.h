#ifndef __LIST_PLAY_H__
#define __LIST_PLAY_H__ 0

#include <pthread.h>

#include "play.h"

/**
 * Struct describing a node of a play list
 */
struct l_p_node {
  struct l_p_node * next;
  struct l_p_node * prev;
  struct play * p;
};

/**
 * Main list of the plays
 * This list include all the play
 */
struct l_p_node * list_play_main;

pthread_mutex_t list_play_mutex;

/* Init a list of plays */
void init_l_p(struct l_p_node ** node);

/* Add a play to the list */
void add_play_to_list(struct l_p_node * node, struct play * p, pthread_mutex_t lock);

/* Get number of elements in the list */
int list_play_length(struct l_p_node * node);

/* Get elements of the list at index in parameters */
struct play * get_play_from_list_at_index(struct l_p_node * node, int i);

/* Destruct the list of team without touching the plays */
void destruct_list_play_light(struct l_p_node * node);

/* Return the first play of list */
struct l_p_node * get_first_play_node_of_list_by_date(struct l_p_node * node);

/* Display all the plays of a list of plays */
void display_list(int fd, struct l_p_node * node);

/* Display all the plays of main list */
void display_main_list(int fd);

/**
 * Remove a play node from a play list without free the play
 * params : struct l_p_node * : The node to remove from the list
 *        : int free_strategy : To decide what to do with memory free
 *                              0 -> No Free
 *                              1 -> Light free (just the node is freed)
 *                              2 -> Heavy free (node and play are freed)
 */
void remove_play_node_from_list(struct l_p_node ** head, struct l_p_node * node, int free_strategy);

/**
 * Add a play node to a list at tail
 * params : struct l_p_node * : The node you want to add
 *        : struct l_p_node * : The list you want the node to be added to
 */
void add_play_node_to_list_at_tail(struct l_p_node * node, struct l_p_node * list);

/* Order the list of plays by date (incremented) */
struct l_p_node * order_list_play_by_date_inc(struct l_p_node ** node);

/* Next function : change current node and teams ones to the next match */
struct l_p_node * next_play(struct l_p_node ** node);

#endif
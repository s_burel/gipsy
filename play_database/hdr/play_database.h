#ifndef __INIT_H__
#define __INIT_H__ 0

#include "play.h"
#include "list_play.h"
#include "league.h"
#include "json_manip.h"

/**
 * This file contains all the file used as interface between the main program
 * and the functions. It contains all the functions used by main to run the
 * program
 */

pthread_mutex_t io_mutex;
pthread_cond_t io_cond;

/**
 * Master read : load the input file into list of play and list ot teams
 */
int load (int fd);

/**
 * Initiate all the data
 * It Create the list of leagues
 * Returns -1 in case of error
 * Returns  0 in case of success
 */
int init (int fd);

/**
 * Load function time recorded
 * params : int as fd to be open by load
 * return : c_time as the time the load took
 */
double load_chrono(int fd);

/**
 * Calculate the average runing time for load
 * params : int as fd to be open by load
 *          int as number of time the load function is run
 * return : double as the average running time
 */
double load_chrono_average(int fd, int n);

/**
 * Destruct all the data to free all the memory used by the program
 */
void clean();

#endif
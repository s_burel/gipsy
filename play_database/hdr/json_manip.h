#ifndef __JSON_MANIP_H__
#define __JSON_MANIP_H__ 0

/**
 * Function to copy a string to itself
 * params : char * to copy
 *          int as the offset to start
 *          int as the number of byte
 * return : char * to itself
 */
char * copy_myself(char * me, int offset, int size);

/**
 * Function to copy a string to itself until it meet the expected char
 * params : char * to copy
 *          int as the offset to start
 *          char as the expected character to stop
 * return : char * to itself
 */
char * copy_myself_until_char(char * me, int offset, char expected);

/**
 * Function to get the n number of an array in json format
 * params : char * of the json format
 *          int as n, (will return the n elemlent)
 *          char * to write the answer
 * return : char * to the answer
 */

char * get_n_case_of_array_json(char * array, int n, char * response);

/**
 * Function that "unstringify" and return value from given key
 *          as a string
 * params : char * pointing to the json string
 *          char * pointing to the name of the key(with "")
 *          char * pointing to the destination
 * return : char * pointing to the destination
 */
char * get_value_from_json(char * json, char * key, char * arg);

/**
 * Function that "unstringify" and return array value from given key
 *          as a string
 * params : char * pointing to the json string
 *          char * pointing to the name of the key(with "")
 *          char * pointing to the destination
 * return : char * pointing to the destination
 */
char * get_array_from_json(char * json, char * key, char * arg);

/**
 * Function that get the league in a play
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_league_from_json(char * json, char * league);

/**
 * Function that get the score in a play
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_score_from_json(char * json, char * arg);

/**
 * Function that get the date of a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_date_from_json(char * json, char * arg);

/**
 * Function that get the day of a date as a string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_day_from_json(char * json, char * arg);

/**
 * Function that get the month of a date as a string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_month_from_json(char * json, char * arg);

/**
 * Function that get the year of a date as a string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_year_from_json(char * json, char * arg);

/**
 * Function that get the teams of a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_teams_from_json(char * json, char * arg);

/**
 * Function that get the home team of a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_home_from_json(char * json, char * arg);

/**
 * Function that get the visitors team of a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_visitor_from_json(char * json, char * arg);

/**
 * Function that get the odds array from a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_odds_from_json(char * json, char * arg);

/**
 * Function that get the home odd from an odds array as a json string
 * params : the char * pointing to the odds array (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_home_odds_from_json(char * json, char * arg);

/**
 * Function that get the draw odd from an odds array as a json string
 * params : the char * pointing to the odds array (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_draw_odds_from_json(char * json, char * arg);

/**
 * Function that get the visitors odd from an odds array as a json string
 * params : the char * pointing to the odds array (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_visitor_odds_from_json(char * json, char * arg);

/**
 * Get a play from a string in json containing multiple plays
 * params : char * pointing to the json list of plays
 *          char * pointing to the destination
 * return : char * pointing to the destination
 */
char * get_play_from_json(char * json, char * arg);

/* Check if a string contain a json play */
int contain_a_full_play(char * json);

/* Return the number of plays contained in a json string */
int number_of_full_play(char * json);

#endif
#ifndef __PLAY_H__
#define __PLAY_H__ 0

#include "json_manip.h"
#include "date.h"
#include "league.h"
#include "team.h"
#include "result.h"
#include "odd.h"
#include "list_team.h"

/**
 * Struct describing a play
 */
struct play {
  struct league * p_league;
  struct date * p_date;
  struct team * p_home;
  struct team * p_visitor;
  struct result * p_result;
  struct odd * p_odd;
};

/* Display the play on given file descriptor */
void display_play(int fd, struct play * p);

/* Struct Construction */
int construct_play(struct play ** p);

/**
 * Build play from json
 * params : struct play * to the struct to be built
 *          char * to the json string with the given informations
 * return : struct play * to the struct once it is built
 */
struct play * build(struct play * p, char * json);

/* Play destructor */
void destruct_play(struct play * p);

/* Destruct all the team of the list */
void destruct_all_plays_of_the_list(struct l_p_node * node);

/* Destruct the list of team and all the teams */
void destruct_list_play(struct l_p_node * node);

/**
 * Return the value for the following question :
 * Does the given play contains at least n match befor the given match ?$
 * params : struct play * : The play we must look the past of teams after
 *          int n         : The number of match needed in the past
 *          int n         : The policy of league :
 *                              if 0 we are looking to match of any league
 *                              if 1 we are only searching for match with
 *                              same league
 * return : 0 if list's past does not contains enough match
 *          1 if list's past contains enough match
 */
int play_teams_contains_at_least_n_previous_match
  (struct play * p, int n, int league_discriminant);


/**
 * Return winner of a play
 * params : struct play * : The result of the match we want the winner of
 * return : struct team * of the winner
 *          return NULL if the play result is a draw
 */
struct team * get_play_winner(struct play * p);

#endif
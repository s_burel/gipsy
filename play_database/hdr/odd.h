#ifndef __ODD_H__
#define __ODD_H__ 0

/**
 * Struct describing an odd
 */

struct odd {
  float home;
  float draw;
  float visitor;
};

/* Odd constructor */
int construct_odd(struct odd ** o);

/* Setter */
void set_odd_value(struct odd * o, float h, float d, float v);

/* Getter */
float get_odd_value(struct odd * o, int v);

/* Odd destructor */
void destruct_odd(struct odd * o);

/* Display the odd on given file descriptor */
void display_odd(int fd, struct odd * o);

#endif
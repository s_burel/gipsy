#ifndef __RESULT_H__
#define __RESULT_H__ 0

/**
 * Struct describing the result of a play
 */

struct result {
  int home;
  int visitor;
  int status;
};

/* Result constructor */
int construct_result(struct result ** r);

/* Check if the char * pointing to the result is valid */
int result_validator(struct result *r, char * s);

/* Result setter */
void set_result_value(struct result *r, char *s);

/* Result destructor */
void destruct_result(struct result * r);

/* Display the team on given file descriptor */
void display_result(int fd, struct result * r);

/**
 * Return an indication of the winner
 * params : struct result * : The result of the match we want the winner of
 * return : An int as an index to the winner
 *             0 -> home is winner
 *             1 -> result is a draw
 *             2 -> visitor is winner
 */
int result_winner(struct result *r);

#endif
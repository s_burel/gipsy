#ifndef __DATE_H__
#define __DATE_H__ 0

/**
 * Struct describing a date
 */

struct date {
  int j;
  int m;
  int a;
};

/* Date constructor */
int construct_date(struct date ** d);

/* Date destructor */
void destruct_date(struct date * d);

/* Display the date on given file descriptor */
void display_date(int fd, struct date * d);

/** Date comparator 
 * Compare two dates
 * params : struct date * d1
 *          struct date * d2
 * return : -1 if d1 < d2
 *           0 if d1 = d2
 *           1 if d1 > d2
 */
int compare_date(struct date * d1, struct date * d2);

#endif
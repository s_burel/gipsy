#ifndef __LEAGUE_H__
#define __LEAGUE_H__ 0

#include <string.h>
#include "list_play.h"

/**
 * Struct describing a league
 */
struct league {
  char * name;
  struct l_p_node * head;
};

/**
 * Declaration of the leagues
 */
struct league list_league[7];

/**
 * That function initiate the list of leagues.
 * Returns : 0 if no errors
 */
int init_l_l(void);

/* Get league from char * */
struct league * get_league_from_string(char * string);

/* Display the league on given file descriptor */
void display_league(int fd, struct league * l);

#endif
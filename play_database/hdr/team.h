#ifndef __TEAM_H__
#define __TEAM_H__ 0

#include "play.h"
#include "list_play.h"

/**
 * Struct describing a team
 */
struct team {
  char * name;
  struct l_p_node * l_p_head;
  pthread_mutex_t l_p_lock;
};

/* Team constructor */
int construct_team(struct team ** t);

/* Team destructor */
void destruct_team(struct team * t);

/* Display the team on given file descriptor */
void display_team(int fd, struct team * t);

/**
 * Return the value for the following question :
 * Does the given team have played enough match in the past ?
 * params : struct team * t         : The team we must look the past from
 *          int n                   : The number of match needed in the past
 *          int league_discriminant : The policy of league :
 *                                      if 0 searching for match of any league
 *                                      if 1 we are only searching for match
 *                                        with same league as current match
 * return : 0 if team's past does not contains enough match
 *          1 if team's past contains enough match
 */
int team_contains_at_least_n_previous_match
  (struct team * t, int n, int league_discriminant);

#endif
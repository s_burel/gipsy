#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "../hdr/team.h"
#include "../hdr/play.h"
#include "../hdr/list_play.h"

/* Team constructor */
int construct_team(struct team ** t) {
  *t = malloc(sizeof(struct team));
  if (*t == 0) {
    perror("Malloc error");
    return EXIT_FAILURE;
  }
  (*t)->name = malloc((sizeof(char))*30);
  init_l_p(&(*t)->l_p_head);
  pthread_mutex_init(&(*t)->l_p_lock,NULL);
  return EXIT_SUCCESS;
}

/* Team destructor */
void destruct_team(struct team * t) {
  destruct_list_play_light(t->l_p_head);
  free(t->name);
  free(t);
}

/* Display the team on given file descriptor */
void display_team(int fd, struct team * t) {
  dprintf(fd,"%s",t->name);
}


/**
 * Return the value for the following question :
 * Does the given team have played enough match in the past ?
 * params : struct team * t         : The team we must look the past from
 *          int n                   : The number of match needed in the past
 *          int league_discriminant : The policy of league :
 *                                      if 0 searching for match of any league
 *                                      if 1 we are only searching for match
 *                                        with same league as current match
 * return : 0 if team's past does not contains enough match
 *          1 if team's past contains enough match
 */
int team_contains_at_least_n_previous_match
  (struct team * t, int n, int league_discriminant) {
    struct l_p_node * play = t->l_p_head;
    struct league * searched_league = play->p->p_league;
    if (n == 0) return 0;
  while(n) {
    if (compare_date(play->prev->p->p_date, play->p->p_date) == -1) return 0;
    play = play->prev;
    if (league_discriminant == 0)
      n--;
    else if (play->p->p_league == searched_league)
      n--;
  }
  return 1;
}
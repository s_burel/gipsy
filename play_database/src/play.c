#include <stdlib.h>
#include <stdio.h>

#include "../hdr/play.h"
#include "../hdr/json_manip.h"
#include "../hdr/date.h"
#include "../hdr/league.h"
#include "../hdr/team.h"
#include "../hdr/result.h"
#include "../hdr/odd.h"
#include "../hdr/list_team.h"

/* Display the play on given file descriptor */
void display_play(int fd, struct play * p) {
  dprintf(fd,"Play Date : ");
  display_date(fd,p->p_date);
  dprintf(fd,"\n  League : ");
  display_league(fd,p->p_league);
  dprintf(fd,"\n  Teams : ");
  display_team(fd,p->p_home);
  dprintf(fd," vs ");
  display_team(fd,p->p_visitor);
  dprintf(fd,"\n  Result : ");
  display_result(fd,p->p_result);
  dprintf(fd,"\n  Odds before play : ");
  display_odd(fd,p->p_odd);
  dprintf(fd,"\n");
}

/* Struct Construction */
int construct_play(struct play ** p) {
  if (*p != 0) {
    perror("Struct play already initialized");
    return EXIT_FAILURE;
  }
  *p = malloc(sizeof(struct play));
  if (*p == NULL) {
    perror("Malloc error");
    return EXIT_FAILURE;
  }
  /* League is not created */
  (*p)->p_league = NULL;
  (*p)->p_home = NULL;
  (*p)->p_visitor = NULL;

  if (construct_date(&(*p)->p_date) == EXIT_FAILURE)
    return EXIT_FAILURE;
  if (construct_result(&(*p)->p_result) == EXIT_FAILURE)
    return EXIT_FAILURE;
  if (construct_odd(&(*p)->p_odd) == EXIT_FAILURE)
    return EXIT_FAILURE;
  return EXIT_SUCCESS;
}

/**
 * Build play from json
 * params : struct play * to the struct to be built
 *          char * to the json string with the given informations
 * return : struct play * to the struct once it is built
 */
struct play * build(struct play * p, char * json) {
  char buffer[50];
  /* League */
  p->p_league = get_league_from_string(get_league_from_json(json,buffer));
  /* Date */
  p->p_date->j = atoi(get_day_from_json(json,buffer));
  p->p_date->m = atoi(get_month_from_json(json,buffer));
  p->p_date->a = atoi(get_year_from_json(json,buffer));
  /* Teams */
  p->p_home = get_team_from_string(get_home_from_json(json,buffer));
  p->p_visitor = get_team_from_string(get_visitor_from_json(json,buffer));
  add_play_to_list(p->p_home->l_p_head,p,p->p_home->l_p_lock);
  add_play_to_list(p->p_visitor->l_p_head,p,p->p_visitor->l_p_lock);
  /* Result */
  set_result_value(p->p_result,get_score_from_json(json,buffer));
  /* Odds */
  set_odd_value(p->p_odd,
    atof(get_home_odds_from_json(json,buffer)),
    atof(get_draw_odds_from_json(json,buffer)),
    atof(get_visitor_odds_from_json(json,buffer)));
  /* Add play to main list */
  add_play_to_list(list_play_main, p, list_play_mutex);
  return p;
}

/* Play destructor */
void destruct_play(struct play * p) {
  destruct_date(p->p_date);
  // destruct_team(p->p_home);
  // destruct_team(p->p_visitor);
  destruct_result(p->p_result);
  destruct_odd(p->p_odd);
  free(p);
}

/* Destruct all the team of the list */
void destruct_all_plays_of_the_list(struct l_p_node * node) {
  struct l_p_node * buffer = node;
  do {
    destruct_play(buffer->p);
    buffer = buffer->next;
  } while (buffer != node);
}

/* Destruct the list of team and all the teams */
void destruct_list_play(struct l_p_node * node) {
  destruct_all_plays_of_the_list(node);
  destruct_list_play_light(node);
}


/**
 * Return the value for the following question :
 * Does the list past contains at least n match befor the given match ?$
 * params : struct l_p_node * : The play we must look the past from
 *          int n             : The number of match needed in the past
 *          int n             : The policy of league :
 *                                  if 0 we are looking to match of any league
 *                                  if 1 we are only searching for match with
 *                                  same league
 * return : 0 if list's past does not contains enough match
 *          1 if list's past contains enough match
 */
int play_teams_contains_at_least_n_previous_match
  (struct play * p, int n, int league_discriminant) {
    struct team * h = p->p_home;
    struct team * v = p->p_visitor;
    int l = league_discriminant;
    if (team_contains_at_least_n_previous_match(h, n, l) == 0 ||
        team_contains_at_least_n_previous_match(v, n, l) == 0)
      return 0;
    return 1;
}


/**
 * Return winner of a play
 * params : struct play * : The result of the match we want the winner of
 * return : struct team * of the winner
 *          return NULL if the play result is a draw
 */
struct team * get_play_winner(struct play * p) {
  int result = result_winner(p->p_result);
  if(result == 0) return p->p_home;
  if(result == 1) return NULL;
  return p->p_visitor;
}
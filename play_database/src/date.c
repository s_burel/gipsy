#include <stdlib.h>
#include <stdio.h>
#include "../hdr/date.h"

/* Date constructor */
int construct_date(struct date ** d) {
  *d = malloc(sizeof(struct date));
  if (*d == 0) {
    perror("Malloc error");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/* Date destructor */
void destruct_date(struct date * d) {free(d);}

/* Display the date on given file descriptor */
void display_date(int fd, struct date * d) {
  dprintf(fd,"%d-%d-%d",d->j,d->m,d->a);
}

/** Date comparator 
 * Compare two dates
 * params : struct date * d1
 *          struct date * d2
 * return : -1 if d1 < d2
 *           0 if d1 = d2
 *           1 if d1 > d2
 */
int compare_date(struct date * d1, struct date * d2) {
  if (d1->a < d2->a) return -1;
  if (d1->a > d2->a) return 1;
  if (d1->m < d2->m) return -1;
  if (d1->m > d2->m) return 1;
  if (d1->j < d2->j) return -1;
  if (d1->j > d2->j) return 1;
  return 0;
}
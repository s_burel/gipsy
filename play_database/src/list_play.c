#include <stdlib.h>
#include "../hdr/play.h"
#include "../hdr/list_play.h"

/**
 * Main list of the plays
 * This list include all the play
 */
struct l_p_node * list_play_main;

/* Init a list of plays */
void init_l_p(struct l_p_node ** node) {
  *node = malloc(sizeof(struct l_p_node));
  (*node)->next = (*node)->prev = *node;
  (*node)->p = NULL;
  pthread_mutex_init(&list_play_mutex,NULL);
}

/* Add a play to the list */
void add_play_to_list(struct l_p_node * node, struct play * p, pthread_mutex_t lock) {
  pthread_mutex_lock(&list_play_mutex);
  // pthread_mutex_lock(&lock);
  if (node->p) { /* A team exist */
    struct l_p_node * new = malloc(sizeof(struct l_p_node));
    new->p = p;
    new->next = node;
    new->prev = node->prev;
    node->prev->next = new;
    node->prev = new;
  } else { /* No team */
    node->p = p;
    node->next = node;
    node->prev = node;
  }
  // pthread_mutex_unlock(&lock);
  pthread_mutex_unlock(&list_play_mutex);
}

/* Get number of elements in the list */
int list_play_length(struct l_p_node * node) {
  if (node == NULL) return 0;
  int i;
  struct l_p_node * buffer = node->next;
  for (i = 1; buffer != node; ++i, buffer = buffer->next);
  return i;
}

/* Get elements of the list at index in parameters */
struct play * get_play_from_list_at_index(struct l_p_node * node, int i) {
  for (; i != 0 ; --i)
    node = node->next;
  return node->p;
}

/* Destruct the list of team without touching the plays */
void destruct_list_play_light(struct l_p_node * node) {
  struct l_p_node * buffer = node;
  struct l_p_node * eraser = node;
  do {
    buffer = buffer->next;
    free(eraser);
    eraser = buffer;
  } while (buffer != node);
}

struct l_p_node * get_first_play_node_of_list_by_date(struct l_p_node * node) {
  struct l_p_node * buffer = node;
  struct l_p_node * minimum = buffer;
  int c;
  do {
      c = compare_date(minimum->p->p_date,buffer->p->p_date);
      if (c > 0 ) minimum = buffer;
      buffer = buffer->next;
  } while (buffer != node);
  return minimum;
}

/**
 * Remove a play node from a play list
 * params : struct l_p_node * : The head of the list
 *        : struct l_p_node * : The node to remove from the list
 *        : int free_strategy : To decide what to do with memory free
 *                              0 -> No Free
 *                              1 -> Light free (just the node is freed)
 *                              2 -> Heavy free (node and play are freed)
 */
void remove_play_node_from_list(struct l_p_node ** head, struct l_p_node * node, int free_strategy) {
  if(*head == node) *head = node->next;
  node->prev->next = node->next;
  node->next->prev = node->prev;
  if (free_strategy)
  node->next = node;
  node->prev = node;
  if(free_strategy) {
    if (free_strategy == 2) /* Free the play */
      destruct_play(node->p);
    free(node); /* Free the node */
  }
}

/**
 * Add a play node to a list at tail
 * params : struct l_p_node * : The node you want to add
 *        : struct l_p_node * : The list you want the node to be added to
 */
void add_play_node_to_list_at_tail(struct l_p_node * node, struct l_p_node * list) {
  node->next = list;
  node->prev = list->prev;
  list->prev->next = node;
  list->prev = node;
}

/* Display all the plays of a list of plays */
void display_list(int fd, struct l_p_node * node) {
  struct l_p_node * buffer = node;
  do {
    display_play(fd,buffer->p);
    buffer = buffer->next;
  } while (buffer != node);
}

/* Display all the plays of main list */
void display_main_list(int fd) {
  display_list(fd,list_play_main);
}


/* Order the list of plays by date (incremented) */
struct l_p_node * order_list_play_by_date_inc(struct l_p_node ** node) {
  struct l_p_node * new = NULL;
  struct l_p_node * buffer;
  int size = list_play_length(*node);
    for (int i = 0; i < size; ++i) {
      buffer = get_first_play_node_of_list_by_date(*node);
      remove_play_node_from_list(node,buffer,0);
      if (new) {
        add_play_node_to_list_at_tail(buffer,new);
      }
      else {
        new = buffer;
        buffer->next = buffer;
        buffer->prev = buffer;
      }
    }
  *node = new;
  return *node;
}


/* Next function : change current node and teams ones to the next match */
struct l_p_node * next_play(struct l_p_node ** node) {
  (*node)->p->p_home->l_p_head = (*node)->p->p_home->l_p_head->next;
  (*node)->p->p_visitor->l_p_head = (*node)->p->p_visitor->l_p_head->next;
  (*node) = (*node)->next;
  return *node;
}
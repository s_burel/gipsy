#include "../hdr/thread.h"
#include <stdio.h>
#include <stdlib.h>

#include "../hdr/play_database.h"
#include "../hdr/play.h"
#include "../hdr/list_play.h"
#include "../hdr/league.h"
#include "../hdr/json_manip.h"
#include "../hdr/thread.h"

/**
 * Construct and build plays from the json_play passed in arguments
 * Function made to be run as a thread routine
 */
void* construct_and_build_play(void* arg) {
  char * json_play = (char *)arg;
  struct play * p;
  p = 0;
  if (construct_play(&p) == EXIT_FAILURE) {
    perror("construct_and_build_play thread -> construct_play error");
    pthread_exit(NULL);
  }
  build(p,json_play);
  // display_play(1,p);
  // printf("\n");
  return(NULL);
}

/**
 * Get a list of plays in argument, create a thread for each of them that will
 * construct a play, build it, and insert it in the plays list
 * params : char * containing a valid list of json plays
 * Function made to be run as a thread routine
 */
void * construct_and_build(void * arg) {
  char * block = (char *) arg;
  int nb_threads = number_of_full_play(block);
  pthread_t * threads = malloc(sizeof(pthread_t)*nb_threads);
  char ** json_play = malloc(sizeof(char *)*nb_threads);
  for (int i = 0; i < nb_threads; ++i)
    json_play[i] = malloc(sizeof(char)*300);

  for (int i = 0; i < nb_threads; ++i) {
    get_play_from_json(block,json_play[i]);
    pthread_create(
      &threads[i],
      NULL,
      construct_and_build_play,
      json_play[i]);
  }

  for (int i = 0; i < nb_threads; ++i)
    pthread_join(threads[i],NULL);

  for (int i = 0; i < nb_threads; ++i)
    free(json_play[i]);
  free(json_play);
  free(threads);
  return(NULL);
}
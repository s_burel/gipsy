#include <stdlib.h>
#include <stdio.h>

#include "../hdr/odd.h"

/* Odd constructor */
int construct_odd(struct odd ** o) {
  *o = malloc(sizeof(struct odd));
  if (*o == 0) {
    perror("Malloc error");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/* Setter */
void set_odd_value(struct odd * o, float h, float d, float v) {
  o->home = h;
  o->draw = d;
  o->visitor = v;
}

/* Getter */
float get_odd_value(struct odd * o, int v) {
  if (v == 0) return o->home;
  if (v == 2) return o->visitor;
  return o->draw;
}

/* Odd destructor */
void destruct_odd(struct odd * o) {free(o);}

/* Display the odd on given file descriptor */
void display_odd(int fd, struct odd * o) {
  dprintf(fd,"%f-%f-%f",o->home,o->draw,o->visitor);
}
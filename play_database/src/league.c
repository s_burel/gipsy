#include <string.h>
#include <stdio.h>
#include "../hdr/league.h"

/**
 * That function initiate the list of leagues.
 * Returns : 0 if no errors
 */
int init_l_l(void){
  list_league[0].name = "Ligue 1";
  list_league[1].name = "Bundesliga";
  list_league[2].name = "Serie A";
  list_league[3].name = "LaLiga";
  list_league[4].name = "Champions League";
  list_league[5].name = "Europa League";
  list_league[6].name = "Premier League";
  return 0;
}

/* Get league from char * */
struct league * get_league_from_string(char * string) {
  if(list_league[0].name == NULL) perror("CRITICAL ERROR : LEAGUE UNINITIALIZED");
  for (int i = 0; i < 7; ++i)
    if (strcmp(string,list_league[i].name) == 0) return &list_league[i];
  dprintf(2,"Error : League Not Found : %s\n", string);
  return 0;
}

/* Display the league on given file descriptor */
void display_league(int fd, struct league * l) {
  dprintf(fd,"%s",l->name);
}
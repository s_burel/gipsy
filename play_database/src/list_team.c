#include <stdlib.h>
#include "../hdr/team.h"
#include "../hdr/list_team.h"

/**
 * Main list of the teams
 * This list include all the teams
 */

/* Init the list_team_main */
void init_l_t() {
  list_team_main = malloc(sizeof(struct l_t_node));
  list_team_main->next = list_team_main->prev = list_team_main;
  list_team_main->t = NULL;
  pthread_mutex_init(&list_team_mutex,NULL);
}

/* Destruct the list of team without touching the teams */
void destruct_list_team_light(struct l_t_node * node) {
  struct l_t_node * buffer = node;
  struct l_t_node * eraser = node;
  do {
    buffer = buffer->next;
    free(eraser);
    eraser = buffer;
  } while (buffer != node);
}

/* Destruct all the team of the list */
void destruct_all_teams_of_the_list(struct l_t_node * node) {
  struct l_t_node * buffer = node;
  do {
    destruct_team(buffer->t);
    buffer = buffer->next;
  } while (buffer != node);  
}

/* Destruct the list of team and all the teams */
void destruct_list_team(struct l_t_node * node) {
  destruct_all_teams_of_the_list(node);
  destruct_list_team_light(node);
}

/**
 * Build and insert a team in list
 * params : struct l_t_node * to the node where to insert the team
 *          char * to the name of the team
 */
void insert_team_in_list(struct l_t_node * node, char * name) {
  pthread_mutex_lock(&list_team_mutex);
  node->prev = list_team_main->prev;
  node->next = list_team_main->next;
  list_team_main->prev->next = node;
  list_team_main->next->prev = node;
  construct_team(&node->t);
  strcpy(node->t->name,name);
  pthread_mutex_unlock(&list_team_mutex);
}

/**
 * Get team from char
 * If the team does not exist, return created one
 * params : char * to the name of the team
 * return : struct league * to the team
 */
struct team * get_team_from_string(char * string) {
  struct l_t_node * buffer;
  buffer = list_team_main;
  /* Team List does not exist */
  if (buffer == NULL) {
    init_l_t();
    buffer = list_team_main;
  }
  /* No team in list */
  if (buffer->t == NULL) {
    insert_team_in_list(buffer,string);
    return buffer->t;
  }
  /* Search for team in list */
  pthread_mutex_lock(&list_team_mutex);
  while (buffer->next != list_team_main) {
    /* Team found */
    if (strcmp(buffer->t->name,string) == 0) {
      /* TODO : Add LRU policy */
      pthread_mutex_unlock(&list_team_mutex);
      return buffer->t;
    }
    buffer = buffer->next;
  }
  pthread_mutex_unlock(&list_team_mutex);

  /* Team not found */
  buffer = malloc(sizeof(struct l_t_node));
  insert_team_in_list(buffer,string);
  return buffer->t;
}

/* Order the play list for each team in the list */
void order_play_list_for_each_team_of_list(struct l_t_node * node) {
  struct l_t_node * buffer = node;
  do {
    order_list_play_by_date_inc(&(buffer->t->l_p_head));
  } while (buffer != node);
}
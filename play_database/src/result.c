#include <stdlib.h>
#include <stdio.h>

#include "../hdr/result.h"

/* Result constructor */
int construct_result(struct result ** r) {
  *r = malloc(sizeof(struct result));
  if (*r == 0) {
    perror("Malloc error");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/* Check if the char * pointing to the result is valid */
int result_validator(struct result *r, char * s) {
  return r->status = 0;
}

/**
 * Return an indication of the winner
 * params : struct result * : The result of the match we want the winner of
 * return : An int as an index to the winner
 *             0 -> home is winner
 *             1 -> result is a draw
 *             2 -> visitor is winner
 */
int result_winner(struct result *r) {
  if (r->home > r->visitor) return 0;
  if (r->home < r->visitor) return 2;
  return 1;
}

/* Result setter */
void set_result_value(struct result *r, char *s) {
  *(s+1) = 0;
  r->home = atoi(s);
  r->visitor = atoi(s+2);
}

/* Result destructor */
void destruct_result(struct result * r) {free(r);}

/* Display the team on given file descriptor */
void display_result(int fd, struct result * r) {
  dprintf(fd,"%d:%d",r->home,r->visitor);
}
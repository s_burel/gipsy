#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../hdr/json_manip.h"

/**
 * Function to copy a string to itself
 * params : char * to copy
 *          int as the offset to start
 *          int as the number of byte
 * return : char * to itself
 */
char * copy_myself(char * me, int offset, int size) {
  strncpy(me,me+offset,size);
  me[size]=0;
  return me;
}

/**
 * Function to copy a string to itself until it meet the expected char
 * params : char * to copy
 *          int as the offset to start
 *          char as the expected character to stop
 * return : char * to itself
 */
char * copy_myself_until_char(char * me, int offset, char expected) {
  int i;
  for (i = 0; me[offset+i] != expected; ++i)
    me[i] = me[offset+i];
  me[i]=0;
  return me;
}

/**
 * Function to get the n number of an array in json format
 * params : char * of the json format
 *          int as n, (will return the n elemlent)
 *          char * to write the answer
 * return : char * to the answer
 */
char * get_n_case_of_array_json(char * array, int n, char * response) {
  int i = 0;
  n = (n*2)+1;
  /* Search for the beggining of the n element */
  while(1) {
    if (array[i] == '"') {
      --n;
      ++i;
      if (n == 0) break;
      else continue;
    } 
    ++i;
  }
  return copy_myself_until_char((array+i),0,'"');
}


/**
 * Function that "unstringify" and return value from given key
 *          as a string
 * params : char * pointing to the json string
 *          char * pointing to the name of the key(with "")
 *          char * pointing to the destination
 * return : char * pointing to the destination
 */
char * get_value_from_json(char * json, char * key, char * arg) {
  char * value;
  int i;
  int size = strlen(key) + 2;
  value = strstr(json,key);
  if (value == NULL) perror("CRITICAL ERROR : JSON FILE CORRUPTED.");
  for (i = size; value[i] != '"'; ++i);
  strncpy(arg,(value+size),i-size);
  arg[i-size]=0;
  return arg;
}

/**
 * Function that "unstringify" and return array value from given key
 *          as a string
 * params : char * pointing to the json string
 *          char * pointing to the name of the key(with "")
 *          char * pointing to the destination
 * return : char * pointing to the destination
 */
char * get_array_from_json(char * json, char * key, char * arg) {
  char * value;
  int i;
  int size = strlen(key) + 2;
  value = strstr(json,key);
  for (i = size; value[i] != ']'; ++i);
  strncpy(arg,(value+size),i-size);
  arg[i-size]=0;
  return arg;
}

/**
 * Function that get the league in a play
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_league_from_json(char * json, char * league) {
  return get_value_from_json(json,"\"league\"",league);
}

/**
 * Function that get the score in a play
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_score_from_json(char * json, char * arg) {
  return get_value_from_json(json,"\"score\"",arg);
}

/**
 * Function that get the date of a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_date_from_json(char * json, char * arg) {
  return get_array_from_json(json,"\"date\"",arg);
}

/**
 * Function that get the day of a date as a string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_day_from_json(char * json, char * arg) {
  get_date_from_json(json,arg);
  return copy_myself_until_char(arg,1,'"');
}

/**
 * Function that get the month of a date as a string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_month_from_json(char * json, char * arg) {
  get_date_from_json(json,arg);
  return copy_myself_until_char(arg,6,'"');
}

/**
 * Function that get the year of a date as a string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_year_from_json(char * json, char * arg) {
  get_date_from_json(json,arg);
  return copy_myself_until_char(arg,11,'"');
}

/**
 * Function that get the teams of a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_teams_from_json(char * json, char * arg) {
  return get_array_from_json(json,"\"teams\"",arg);
}

/**
 * Function that get the home team of a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_home_from_json(char * json, char * arg) {
  get_array_from_json(json,"\"teams\"",arg);
  return copy_myself_until_char(arg, 1,'"');
}

/**
 * Function that get the visitors team of a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_visitor_from_json(char * json, char * arg) {
  int i;
  get_array_from_json(json,"\"teams\"",arg);
  for (i = strlen(arg)-2; arg[i] != '"'; --i);
  return copy_myself_until_char(arg,i+1,'"');
}

/**
 * Function that get the odds array from a play as a json string
 * params : the char * pointing to the play (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_odds_from_json(char * json, char * arg) {
  get_array_from_json(json,"\"cote\"",arg);
  return arg;
}

/**
 * Function that get the home odd from an odds array as a json string
 * params : the char * pointing to the odds array (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_home_odds_from_json(char * json, char * arg) {
  return get_n_case_of_array_json(get_odds_from_json(json,arg),0,arg);
}

/**
 * Function that get the draw odd from an odds array as a json string
 * params : the char * pointing to the odds array (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_draw_odds_from_json(char * json, char * arg) {
  return get_n_case_of_array_json(get_odds_from_json(json,arg),1,arg);
}

/**
 * Function that get the visitors odd from an odds array as a json string
 * params : the char * pointing to the odds array (json format)
 *          the char * pointing to the destination
 * return : A char * pointing to the destination
 */
char * get_visitor_odds_from_json(char * json, char * arg) {
  return get_n_case_of_array_json(get_odds_from_json(json,arg),2,arg);
}

/**
 * Get a play from a string in json containing multiple plays
 * params : char * pointing to the json list of plays
 *          char * pointing to the destination
 * return : char * pointing to the destination
 */
char * get_play_from_json(char * json, char * arg) {
  int i = 0;
  int j = 0;
  /* Read the Play until first {*/
  while(json[i] != '{')
    ++i;
  json[i] = '#';
  /* Read the Play until } */
  while(json[i] != '}') {
    arg[j] = json[i];
    ++i;
    ++j;
  }
  json[i] = '#';
  arg[j] = 0;
  return arg;
}

/* Check if a string contain a json play */
int contain_a_full_play(char * json) {
  if ((strchr(json,'}') != NULL) && (strchr(json,'{') != NULL))
    return 1;
  else return 0;
}

/* Return the number of plays contained in a json string */
int number_of_full_play(char * json) {
  int i = 0;
  char * offset = json;
  while (1) {
    if ((offset = strchr(offset,'{')) == NULL) break;
    if ((offset = strchr(offset,'}')) == NULL) break;
    i++;
  }
  return i;
}
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <unistd.h>

#include "../hdr/play_database.h"
#include "../hdr/play.h"
#include "../hdr/list_play.h"
#include "../hdr/league.h"
#include "../hdr/json_manip.h"
#include "../hdr/thread.h"

/**
 * This file contains all the file used as interface between the main program
 * and the functions. It contains all the functions used by main to run the
 * program
 */

/**
 * Master read : load the input file into list of play and list ot teams
 */
int load (int fd) {
  int c;
  char block[1000];
/* Initial read */
  c = read(fd,block,1000);
  while (c > 0) {
    if(c < 1000) *(block+c)=0;
  /* Read and build all play in the block red */
    pthread_t thread;
    pthread_create(
      &thread,
      NULL,
      construct_and_build,
      block);
    // if (construct_and_build(block) == EXIT_FAILURE) return EXIT_FAILURE;
    pthread_join(thread,NULL);
  /* In case the file is red until its end : stop*/
    if(c < 1000) break;
  /* Get the last occurence of the play red to load matching offset */
    c = (int)((strrchr(block,'#')) - block);
    lseek(fd, c-999, SEEK_CUR);
  /* Read for the next block */
    c = read(fd,block,1000);
  }
  return EXIT_SUCCESS;
}

/**
 * Load function time recorded
 * params : int as fd to be open by load
 * return : c_time as the time the load took
 */
double load_chrono(int fd){
  /* Record time to calculate runtime */
  clock_t start_t, end_t;
  start_t = clock();
  
  load(fd);

  /* Stop the clock and display runtime */
  end_t = clock();
  // printf("Total time taken by CPU: %f\n", difftime(end_t, start_t)/ CLOCKS_PER_SEC);
  // printf("Exiting of the program...\n");
  return (difftime(end_t, start_t)/ CLOCKS_PER_SEC);
}

/**
 * Calculate the average runing time for load
 * params : int as fd to be open by load
 *          int as number of time the load function is run
 * return : double as the average running time
 */
double load_chrono_average(int fd, int n) {
  double average = 0;
  for (int i = 0; i < n; ++i) {
      lseek(fd,0,SEEK_SET);
      init(fd);
      average += load_chrono(fd);
      clean();
  }
  return average/n;
}

/**
 * Initiate all the data
 * It Create the list of leagues
 * Returns -1 in case of error
 * Returns  0 in case of success
 */
int init (int fd) {
/* Create the list of leagues */
init_l_l();
/* Init the list of plays */
init_l_p(&(list_play_main));
/* Init the list of teams */
init_l_t();
/* Init the io mutex */
pthread_mutex_init(&io_mutex,NULL);
pthread_cond_init(&io_cond,NULL);
  return 0;
}

/**
 * Destruct all the data to free all the memory used by the program
 */
void clean() {
  destruct_list_play(list_play_main); 
  destruct_list_team(list_team_main);
}
#ifndef __DATA_CONTROL_H__
#define __DATA_CONTROL_H__ 0

#define PARAMATER_NUMBER_OF_VALUE 100
#define PARAM_MEMBER             value

/**
 * struct parameter
 * This struct describe a list of int used by a prediction algorithm
 * as parameters to define its behaviour
 */
struct parameter {
  int rate;
  int value[PARAMATER_NUMBER_OF_VALUE];
};

/* Allocate and initialize a matrix of parameters */
struct parameter ** alloc_parameters_matrix(int size);

/* Randomize a parameter */
void randomize_parameter(struct parameter * p);

/* Function to start a new rand */
void set_seed();

/* Function to get a seed for a rand */
int get_seed();

/* Display one parameter as a matrix */
void display_parameter(int fd, struct parameter * p);

/* Set value of the parameter at given index */
void parameter_set_value(struct parameter * p, int index, int value);

/* Get value of the parameter at given index */
int parameter_get_value(struct parameter * p, int index);

/**
 * Generate a new parameter from two parents parameters
 * params : struct parameter * son    : The new generated parameter
 *          struct parameter * father : The first parent
 *          struct parameter * mother : The second parent
 *          int mutation              : Flag to allow mutation
 *                                      If set to 0, the son is just a mix
 *                                        between the parents
 *                                      If set to 1, the son is a mix of
 *                                        the parent and a random value
 * return : struct parameter * As the son, the new generated parameter
 **/
struct parameter * birth_of_parameter(
  struct parameter * son,
  struct parameter * father,
  struct parameter * mother,
  int mutation);

#endif
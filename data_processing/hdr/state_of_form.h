#ifndef __STATE_H__
#define __STATE_H__ 0

#include "../hdr/parameter.h"
#include <play_database.h>

#define LEAGUE_DISCRIMINANT      PARAM_MEMBER[0]
#define NB_MATCH                 PARAM_MEMBER[1]
#define RANGE_VALUE              PARAM_MEMBER[2]
#define REVERSE_CALCULUS         PARAM_MEMBER[3]
#define SIZE                     25
#define VALUE_WIN_ARRAY          PARAM_MEMBER + 5
#define VALUE_DRAW_ARRAY         PARAM_MEMBER + 30
#define VALUE_LOOSE_ARRAY        PARAM_MEMBER + 55
#define RATIO_MAX                10000
#define HOME_VISITOR_WIN_RATIO   PARAM_MEMBER[80]
#define HOME_VISITOR_DRAW_RATIO  PARAM_MEMBER[81]
#define HOME_VISITOR_LOOSE_RATIO PARAM_MEMBER[82]
/**
 * Initiate parameter range for given function
 * params : struct parameter * : The parameter we are setting the range to
 *          function           : The function that will use the parameter
 */
struct parameter * set_parameter_for_given_function
  (struct parameter * behavior, void * fn());

/**
 * Change the given parameter to fit calculate_team_state function
 * params : struct parameter * : The parameter we are changing
 * return : struct parameter * : The changed parameter
 */
struct parameter * set_parameter_for_calculate_team_state
  (struct parameter * behavior);


/**
 * This algorithm is used to predict an odd to next play accorded to the
 * team state of form
 * params : struct * odd       : as the result of the calculus
 *          struct * parameter : as the behaviour of the algorithm
 *          struct * play      : the play the algorithm must predict
 *                               the odds for
 * return : int : 0 if the calculus is done without any errors
 *                1 if the algorthm didn't had enough match in the past to
 *                  dedict the future
 */
int calculate_team_state_for_given_play
  (struct odd * o, struct parameter * behavior, struct play * p);

/**
 * This algorithm is used to predict an odd to next play for one
 * team passed in argument
 * params : struct * odd       : as the result of the calculus
 *          struct * parameter : as the behaviour of the algorithm
 *          struct * team      : the team the algorithm must predict
 *                               the odds for
 * return : int : 0 if the calculus is done without any errors
 *                1 if the algorthm didn't had enough match in the past to
 *                  dedict the future
 */
int calculate_team_state_for_given_team
  (struct odd * o, struct parameter * behavior, struct team * t);


/**
 * Set new odd as mix of two other odds
 * params : struct odd * on      : The new struct mix between two other odds
 *          struct odd * hn      : The struct odd of the home team
 *          struct odd * vn      : The struct odd of the visitor team
 *          struct parameter * p : The behavior of the algorithm
 */
void set_state
  (struct odd * on, struct odd * oh, struct odd * ov, struct parameter * p);

#endif
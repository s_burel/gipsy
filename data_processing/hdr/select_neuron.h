#ifndef __SELECT_NEURON_H__
#define __SELECT_NEURON_H__ 0

#define NEURON_RATIO_MAX  PARAM_MEMBER[0]
#define NUMBER_OF_NEURONS PARAM_MEMBER[1]
#define STATE_OF_FORM     PARAM_MEMBER[2]

struct parameter * set_parameter_for_select_neutron
  (struct parameter * behavior);

/* Temporary function, now useless, but needed in case of many neurons */
struct odd * neurons_to_output (struct odd * state_of_form);

#endif
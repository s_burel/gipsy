#ifndef __MAIN_AI_H__
#define __MAIN_AI_H__ 0

#include <play_database.h>


/**
 * This file contains the headers of the function dealing with the ai
 */

struct ai {
  struct param_array * state_of_form;
  struct param_array * select;  
};

struct ai * construct_ai(int size);

void destruct_ai(struct ai * a);

/* Records nodes intel, give them to select_node and return its result */
struct odd * get_output(struct ai * m);

/* Run the calculus for each nodes */
void calculate_for_one_play(struct ai * m, struct l_p_node * node);

/* Sanctionate the behaviors of every neurons according to actual results */
void sanctionate_every_neurons(struct ai * m, struct l_p_node * node);

/* New Generation */
void new_generation(struct ai * m, struct l_p_node * node);

#endif
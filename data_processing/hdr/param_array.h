#ifndef __PARAM_ARRAY_H__
#define __PARAM_ARRAY_H__ 0

#include "parameter.h"
#include <play_database.h>

#define PARAMETER_DEFAULT_VALUE 1000
#define GENETIC_DEATH           30
#define GENETIC_NEW_RAND        10
#define GENETIC_NEW_BORN        10
#define GENETIC_MUTANT          10

/**
 * State of form struct
 * Contains list of parameters
 * member : int                 : number of parameters, size array
 *          struct odd **       : array of struct odds, 
 *                                used to store algorithm results
 *          struct parameter ** : array of parameter
 *          int *               : array of int used to store a value
 *                                used to rate parameters
 *          int *               : array of int used to store the result
 *                                of the last calculs
 */
struct param_array {
  int size;
  struct odd ** odds;
  struct parameter ** behaviors;
  int * value;
  int * last_result;
};

struct param_array * construct_param_array(int size);

void destruct_param_array(struct param_array * p);

struct odd * return_best_behavior_of_the_param_array(struct param_array * p);

/* Sanctionate the behaviors according to actual results */
void sanctionate_param_array(struct param_array * param, struct play * play);

/* Order an array */
void order_array(struct param_array * param, int * array, int size);

/* Reset and randomize a parameter at given index */
void randomize_at(struct param_array * p, int index);

/**
 * Function that create a new generation of behaviours
 * The best behaviour are kepted
 * The worst are removed
 * New behaviours are created
 *   Part of them are total random
 *   Part of them are mix from best behaviors
 *   Part of them are mix from rand and best behaviors
 * params : struct param_array * param : The list of param we are updating
 *          void * callback ()         : The function to reset the value of
 *                                       a given behaviour
 */
void natural_selection(struct param_array * param, void * callback ());

/* Run a function given in parameter for eache struct of the array */
void for_each_parameter(struct param_array * pa, void * callback ());

#endif
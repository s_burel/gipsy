#include "../hdr/param_array.h"
#include <stdlib.h>
#include <stdio.h>

/**
 * State of form struct
 * Contains list of parameters
 * member : int                 : number of parameters, size array
 *          struct odd **       : array of struct odds, 
 *                                used to store algorithm results
 *          struct parameter ** : array of parameter
 *          int *               : array of int used to store a value
 *                                used to rate parameters
 *          int *               : array of int used to store the result
 *                                of the last calculs
 */
struct param_array * construct_param_array(int size) {
  struct param_array * p = malloc(sizeof(struct param_array));
  p->size = size;
  p->odds = malloc(sizeof(struct odd *) * size);
  p->behaviors = alloc_parameters_matrix(size);
  p->value = malloc(sizeof(int) * size);
  p->last_result = calloc (size, sizeof(int));
  for (int i = 0; i < size; ++i) {
    p->odds[i] = malloc(sizeof(struct odd *));
    p->value[i] = PARAMETER_DEFAULT_VALUE;
  }
  return p;
}


/* Run a function given in parameter for eache struct of the array */
void for_each_parameter(struct param_array * pa, void * callback ()) {
  for (int i = 0; i < pa->size; ++i) {
    callback(pa->behaviors[i]);
  }
}

void destruct_param_array(struct param_array * p) {
  free(p->value);
  free(p->last_result);
  for (int i = 0; i < p->size; ++i) {
    free(p->behaviors[i]);
    free(p->odds[i]);
  }
  free(p->behaviors);
  free(p->odds);
  free(p);
  return;
}

struct odd * return_best_behavior_of_the_param_array(struct param_array * p) {
  struct odd * best = p->odds[0];
  int v = p->value[0];
  int i = 1;
  while (i < p->size) {
    if (p->value[i] > v) {
      v = p->value[i];
      best = p->odds[i];
    }
    i++;
  }
  return best;
}


/* Sanctionate the behaviors according to actual results */
void sanctionate_param_array(struct param_array * param, struct play * play) {
  struct odd * bookie = play->p_odd;
  struct odd ** odds = param->odds;
  int *v = param->value;
  int *lr = param->last_result;
  int i = 0;
  int winner = result_winner(play->p_result);
  /* For each behaviors of the param_array : */
  while (i < param->size) {
    /* If the the result is not valid */
    if (lr[i] != 0) {
      v[i]--;
    } else { /* Result valid */
      /* If the algorithm predicted an higher chance of success *
       * for the winner than the bookmakers odds then the       *
       * algorithm value increase, otherwise, its decrease      */
      if (get_odd_value(odds[i], winner) < get_odd_value(bookie, winner))
        v[i] += 10;
      else v[i] -= 10;
    }
    i++;
  }
  return;
}

/* Order an array */
void order_array(struct param_array * param, int * array, int size) {
  int j;
  int buffer;
  /* For eache element of the array */
  for (int i = 0; i < size -1 ; ++i) {
    /* If the current is bigger than the next, then : */
    if (param->value[array[i]] > param->value[array[i+1]]) {
      j = i;
      /* We switch next and current until the next is bigger */
      while(param->value[array[j+1]] < param->value[array[j]] && j > 0 ) {
        buffer = array[j];
        array[j] = array[j+1];
        array[j+1] = buffer;
        j--;
      }
      while
        (param->value[array[j]] > param->value[array[j+1]] && j < (size -2)) {
        buffer = array[j];
        array[j] = array[j+1];
        array[j+1] = buffer;
        j++;
      }
    }
  }
}

/**
 * Function that create a new generation of behaviours
 * The best behaviour are kepted
 * The worst are removed
 * New behaviours are created
 *   Part of them are total random
 *   Part of them are mix from best behaviors
 *   Part of them are mix from rand and best behaviors
 * params : struct param_array * param : The list of param we are updating
 *          void * callback ()         : The function to reset the value of
 *                                       a given behaviour
 */
void natural_selection(struct param_array * param, void * callback ()) {
  int size = param->size;
  int death = GENETIC_DEATH;
  int new_rand = GENETIC_NEW_RAND;
  int new_born = GENETIC_NEW_BORN;
  int mutant = GENETIC_MUTANT;
  /* This array is a fifo list recording the index of the worst behaviors *
   * in the parm_array struct                                             */
  // int * worst = malloc(sizeof(int) * death);
  int worst[GENETIC_DEATH];
  for (int i = 0; i < death; ++i)
    worst[i] = i;
  /* Order the array */
  order_array(param, worst, death);
  /* Search for the worst behaviors in the list */
  for (int i = death; i < size; ++i) {
    if (param->value[i] < param->value[worst[death - 1]]) {
      worst[death - 1] = i;
      order_array(param, worst, death);
    }
  }
  /* Generate new : */
  int i = 0;
  for (int j = 0; j < new_rand; ++j)
    randomize_at(param, worst[i++]);
  for (int j = 0; j < new_born; ++j)
    birth_of_parameter(
      param->behaviors[worst[i++]],
      /* Random selection of parents */
      param->behaviors[rand() % param->size],
      param->behaviors[rand() % param->size],
      /* No mutation */
      0);
  for (int j = 0; j < mutant; ++j)
    birth_of_parameter(
      param->behaviors[worst[i++]],
      /* Random selection of parents */
      param->behaviors[rand() % param->size],
      param->behaviors[rand() % param->size],
      /* Mutation allowed */
      1);
  for (i = 0; i < death; ++i) {
    /* Set range and reset the value */
    callback(param->behaviors[worst[i]]);
    param->value[worst[i]] = PARAMETER_DEFAULT_VALUE;
  }
  // free(worst);
  // for (int i = 0; i < count; ++i);
}

/* Reset and randomize a parameter at given index */
void randomize_at(struct param_array * p, int index) {
  struct parameter * target = p->behaviors[index];
  randomize_parameter(target);
  p->value[index] = PARAMETER_DEFAULT_VALUE;
}
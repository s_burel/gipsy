#include "../hdr/state_of_form.h"

/**
 * Initiate parameter range for given function
 * params : struct parameter * : The parameter we are setting the range to
 *          function           : The function that will use the parameter
 */
struct parameter * set_parameter_for_given_function
  (struct parameter * behavior, void * fn()) {
  if (fn == (void *)calculate_team_state_for_given_play)
    set_parameter_for_calculate_team_state(behavior);
  return behavior;
}

/**
 * Set the value of each part of an array of result in a struct parameter
 * describing the behaviour of a state of form algorithm
 * params : int * array     : Pointer to the struct array (VALUE_WIN_ARRAY or
 *                          VALUE_DRAW_ARRAY or VALUE_LOOSE_ARRAY)
 *          int range_value : The maximum value of the array
 *          int nb_match    : The range of the array
 */
void set_range_array (int * array, int range_value, int * nb_match) {
  for (int j = 0; j < *nb_match; ++j) {
    if (range_value == 0) {
      *nb_match = 0;
      return;
    }
    array[j] %= range_value;
    if (array[j] == 0) {
      *nb_match = j; /* Change number of cared match in parameter */
      break;
    }
    range_value -= array[j];
    if(range_value > array[j])
      range_value = array[j]; /* To be sure to get decremented values on match values */
  }
}

/**
 * Change the given parameter to fit calculate_team_state function
 * params : struct parameter * : The parameter we are changing
 * return : struct parameter * : The changed parameter
 */
struct parameter * set_parameter_for_calculate_team_state
  (struct parameter * behavior) {
  /* Declaration of the value arrays */
  int * value_of_win = behavior->VALUE_WIN_ARRAY;
  int * value_of_draw = behavior->VALUE_DRAW_ARRAY;
  int * value_of_loose = behavior->VALUE_LOOSE_ARRAY;
  /* Does the compared match must be part of the same league ? */
  behavior->LEAGUE_DISCRIMINANT %= 2;
  /* Does we must compare visitor odds or visitors with home ? */
  behavior->REVERSE_CALCULUS %=2;
  /* Number of match we look in the past (max 25) */
  behavior->NB_MATCH %= SIZE;
  /* Change the ratio */
  behavior->HOME_VISITOR_WIN_RATIO %= RATIO_MAX;
  behavior->HOME_VISITOR_DRAW_RATIO %= RATIO_MAX;
  behavior->HOME_VISITOR_LOOSE_RATIO %= RATIO_MAX;
  /* Total value of match (used as a coefficient to compare to, max is 10000) */
  behavior->RANGE_VALUE %= 10000;
  /* Value for each win (chronological order) */
  set_range_array(value_of_win, behavior->RANGE_VALUE, &behavior->NB_MATCH);
  set_range_array(value_of_draw, behavior->RANGE_VALUE, &behavior->NB_MATCH);
  set_range_array(value_of_loose, behavior->RANGE_VALUE, &behavior->NB_MATCH);
  return behavior;
}

/* Calculate the float for set state function */
float calculus(float first, float second, float ratio) {
  float r;
  r = (1.0 / first * ratio);
  r += (1.0 / second * (1.0 - ratio));
  r = 1.0 / r;
  return r;
}


/**
 * Set new odd as mix of two other odds
 * params : struct odd * on      : The new struct mix between two other odds
 *          struct odd * hn      : The struct odd of the home team
 *          struct odd * vn      : The struct odd of the visitor team
 *          struct parameter * p : The behavior of the algorithm
 */
void set_state
  (struct odd * on, struct odd * oh, struct odd * ov, struct parameter * p) {
  float h, d, v, ratio;
  if (p->REVERSE_CALCULUS) {
    ratio = (float)p->HOME_VISITOR_WIN_RATIO / (float)RATIO_MAX;
    h = calculus(oh->home, ov->visitor, ratio);
    ratio = (float)p->HOME_VISITOR_LOOSE_RATIO / (float)RATIO_MAX;
    v = calculus(oh->visitor, ov->home, ratio);
  } else {
    ratio = (float)p->HOME_VISITOR_WIN_RATIO / (float)RATIO_MAX;
    h = calculus(oh->home, ov->home, ratio);
    ratio = (float)p->HOME_VISITOR_LOOSE_RATIO / (float)RATIO_MAX;
    v = calculus(oh->visitor, ov->visitor, ratio);
  }
  /* Set draw odd value */
  ratio = (float)p->HOME_VISITOR_DRAW_RATIO/(float)RATIO_MAX;
  d = calculus(oh->draw, ov->draw, ratio);
  /* Set odd values */
  set_odd_value(on, h, d, v);
}


/**
 * This algorithm is used to predict an odd to next play accorded to the
 * team state of form
 * params : struct * odd       : as the result of the calculus
 *          struct * parameter : as the behaviour of the algorithm
 *          struct * play      : the play the algorithm must predict
 *                               the odds for
 * return : int : 0 if the calculus is done without any errors
 *                1 if the algorthm didn't had enough match in the past to
 *                  dedict the future
 */
int calculate_team_state_for_given_play
  (struct odd * o, struct parameter * behavior, struct play * p) {
  /* Does the compared match must be part of the same league ? */

  if (play_teams_contains_at_least_n_previous_match(
      p, behavior->NB_MATCH, behavior->LEAGUE_DISCRIMINANT) == 0) {
    set_odd_value(o, 3, 3, 3);
    return 1;
  }

  struct odd h;
  struct odd v;
  calculate_team_state_for_given_team(&h, behavior, p->p_home);
  calculate_team_state_for_given_team(&v, behavior, p->p_visitor);

  set_state(o, &h, &v, behavior);
  return 0;
}

/**
 * This algorithm is used to predict an odd to next play for one
 * team passed in argument
 * params : struct * odd       : as the result of the calculus
 *          struct * parameter : as the behaviour of the algorithm
 *          struct * team      : the team the algorithm must predict
 *                               the odds for
 * return : int : 0 if the calculus is done without any errors
 *                1 if the algorthm didn't had enough match in the past to
 *                  dedict the future
 */
int calculate_team_state_for_given_team
  (struct odd * o, struct parameter * behavior, struct team * t) {
  struct l_p_node * n = t->l_p_head;
  struct league * l = n->p->p_league;
  struct team * winner;
  int win_value = 1;
  int draw_value = 1;
  int loose_value = 1;
  int nb = behavior->NB_MATCH;
  int i = 0;

  while (i < nb){
    if (n->p->p_league != l && behavior->LEAGUE_DISCRIMINANT == 1) {
      n = n->prev;
      continue;
    }
    winner = get_play_winner(n->p);
    if (winner == t)
      win_value += *(behavior->VALUE_WIN_ARRAY+i);
    else if (winner == NULL)
      draw_value += *(behavior->VALUE_DRAW_ARRAY+i);
    else loose_value += *(behavior->VALUE_DRAW_ARRAY+i);
    i++;
    n = n->prev;
  }

  int sum = win_value + draw_value + loose_value;

  set_odd_value(o,
    (1.0/win_value * sum),
    (1.0/draw_value * sum),
    (1.0/loose_value * sum));

  return 0;
}
#include "../hdr/main_ai.h"
#include "../hdr/param_array.h"
#include "../hdr/select_neuron.h"
#include "../hdr/state_of_form.h"
#include <stdlib.h>

/**
 * This file contains the source of the function dealing with the ai
 *
 *
 * struct ai {
 *   struct param_array * state_of_form;
 *   struct param_array * select;  
 * };
 */

struct ai * construct_ai(int size) {
  struct ai * a = malloc(sizeof(struct ai));
  a->state_of_form = construct_param_array(size);
  a->select = construct_param_array(size);
  for_each_parameter(a->state_of_form, (void * )set_parameter_for_calculate_team_state);
  for_each_parameter(a->select, (void * )set_parameter_for_select_neutron);
  return a;
}

void destruct_ai(struct ai * a) {
  destruct_param_array(a->state_of_form);
  destruct_param_array(a->select);
  free(a);
}

/* Records nodes intel, give them to select_node and return its result */
struct odd * get_output(struct ai * m) {
  struct odd * sof;
  struct odd * out;
  sof = return_best_behavior_of_the_param_array(m->state_of_form);
  out = neurons_to_output(sof);
  return out;
}

/* Run the calculus for each nodes */
void calculate_for_one_play(struct ai * m, struct l_p_node * node) {
  struct play * play = node->p;
  struct odd * odd;
  struct parameter * param;
  int * ret;
  /* For each behaviors of the state of form neuron */
  for (int i = 0; i < m->state_of_form->size; ++i) {
    odd = m->state_of_form->odds[i];
    param = m->state_of_form->behaviors[i];
    ret = (m->state_of_form->last_result + i);
    *ret = calculate_team_state_for_given_play(odd, param, play);
  }
}

/* Sanctionate the behaviors of every neurons according to actual results */
void sanctionate_every_neurons(struct ai * m, struct l_p_node * node) {
  struct play * p = node->p;
  sanctionate_param_array(m->state_of_form, p);
}

/* New Generation */
void new_generation(struct ai * m, struct l_p_node * node) {
  natural_selection(
    m->state_of_form,
    (void * )set_parameter_for_calculate_team_state);
  natural_selection(
    m->select,
    (void * )set_parameter_for_select_neutron);
}
#include "../hdr/parameter.h"
#include "../hdr/select_neuron.h"

/**
 * Initiate parameter range for given function
 * params : struct parameter * : The parameter we are setting the range to
 *          function           : The function that will use the parameter
 */

/**
 * Change the given parameter to fit calculate_team_state function
 * params : struct parameter * : The parameter we are changing
 * return : struct parameter * : The changed parameter
 */
struct parameter * set_parameter_for_select_neutron
  (struct parameter * behavior) {
  behavior->STATE_OF_FORM = 1;
  return behavior;
}

/* Temporary function, now useless, but needed in case of many neurons */
struct odd * neurons_to_output (struct odd * state_of_form) {
  return state_of_form;
}
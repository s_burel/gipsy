#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <play_database.h>
#include "../hdr/parameter.h"

/* Allocate and initialize a matrix of parameters */
struct parameter ** alloc_parameters_matrix(int size) {
  struct parameter ** new = malloc(sizeof(struct parameter) * size);
  for (int i = 0; i < size; ++i) {
    new[i] = malloc(sizeof(struct parameter));
    randomize_parameter(new[i]);
  }
  return new;
}

/* Randomize a parameter */
void randomize_parameter(struct parameter * p) {
  for (int i = 0; i < PARAMATER_NUMBER_OF_VALUE; ++i) {
    p->value[i] = rand();
  }
}

/**
 * Generate a new parameter from two parents parameters
 * params : struct parameter * son    : The new generated parameter
 *          struct parameter * father : The first parent
 *          struct parameter * mother : The second parent
 *          int mutation              : Flag to allow mutation
 *                                      If set to 0, the son is just a mix
 *                                        between the parents
 *                                      If set to 1, the son is a mix of
 *                                        the parent and a random value
 * return : struct parameter * As the son, the new generated parameter
 **/
struct parameter * birth_of_parameter(
  struct parameter * son,
  struct parameter * father,
  struct parameter * mother,
  int mutation) {
  int random = rand();
  /* Generate the son without mutation */
  for (int i = 0; i < PARAMATER_NUMBER_OF_VALUE; ++i) {
    if (random == 0) random = rand();
    if (rand() % 2)
      parameter_set_value(son, i, parameter_get_value(father,i));
    else
      parameter_set_value(son, i, parameter_get_value(mother,i));
    random = random >> 1;
  }
  /* Mutate if needed */
  if (mutation == 1) {
    for (int i = 0; i < PARAMATER_NUMBER_OF_VALUE; ++i) {
      if (random == 0) random = rand();
      if (rand() % 2)
        parameter_set_value(son, i, rand());
      random = random >> 1;
    }
  }
  return son;
}

/* Function to get a seed for a rand */
int get_seed(void) {
  return (int) (getpid()*getppid());
}

/* Function to start a new rand */
void set_seed(void) {
  srand((unsigned int)get_seed());
}

/* Set value of the parameter at given index */
void parameter_set_value(struct parameter * p, int index, int value) {
  p->value[index] = value;
}

/* Get value of the parameter at given index */
int parameter_get_value(struct parameter * p, int index) {
  return p->value[index];
}

/* Display one parameter as a matrix */
void display_parameter(int fd, struct parameter * p) {
  for (int i = 0; i < PARAMATER_NUMBER_OF_VALUE; ++i)
    dprintf(fd,"%d\t",p->value[i]);
  dprintf(fd,"\n");
}
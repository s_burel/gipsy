#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <play_database.h>
#include <main_ai.h>

// #include <parameter.h>
// #include <state_of_form.h>
#include <param_array.h>

#include <stdlib.h>

int main(int argc, char const *argv[])
{

set_seed();

  printf("Load all the game : \n");

  /* Initiate all the data */
  int fd = open("raw_data/ligue1.txt",O_RDONLY);
  init(fd);

  /* Load add the play from the file descriptor from parameter */
  load(fd);
  close(fd);

  fd = open("raw_data/bundesliga.txt",O_RDONLY);
  load(fd);
  close(fd);

  fd = open("raw_data/champions_league.txt",O_RDONLY);
  load(fd);
  close(fd);

  fd = open("raw_data/europa_league.txt",O_RDONLY);
  load(fd);
  close(fd);

  fd = open("raw_data/laliga.txt",O_RDONLY);
  load(fd);
  close(fd);

  fd = open("raw_data/premier_league.txt",O_RDONLY);
  load(fd);
  close(fd);

  fd = open("raw_data/serie_a.txt",O_RDONLY);
  load(fd);
  close(fd);


  printf("Order the list of plays : \n");
  order_list_play_by_date_inc(&list_play_main);

  printf("Order the teams list of plays : \n");
  order_play_list_for_each_team_of_list(list_team_main);

  printf("Construction of the ai : \n");
  struct ai * ai;
  ai = construct_ai(1000);

  struct odd * o;

int i = 0;

float walleth = 100;
float walletd = 100;
float walletv = 100;
float wallet = 0;
float ratio_de_mise = 0.01;

while (1) {
  printf("*** Iteration %d ***\n",i);
  printf("Press Enter to Continue\n");
  getchar();

  for (int i = 0; i < ai->state_of_form->size; ++i)
    printf("%d ", ai->state_of_form->value[i]);
  printf("\n");

  calculate_for_one_play(ai, list_play_main);
  o = get_output(ai);

  display_odd(2,o);
  printf("\n");
  display_odd(2,list_play_main->p->p_odd);
  printf("\n");
  if (o->draw < list_play_main->p->p_odd->draw) {
    if (result_winner(list_play_main->p->p_result) == 1)
      walletd += (((walletd * ratio_de_mise) * list_play_main->p->p_odd->draw)-(walletd * ratio_de_mise));
    else walletd -= (walletd * ratio_de_mise);
  }
  if (o->home < list_play_main->p->p_odd->home) {
    if (result_winner(list_play_main->p->p_result) == 0)
      walleth += (((walleth * ratio_de_mise) * list_play_main->p->p_odd->draw)-(walleth * ratio_de_mise));
    else walleth -= (walleth * ratio_de_mise);
  }
  if (o->visitor < list_play_main->p->p_odd->visitor) {
    if (result_winner(list_play_main->p->p_result) == 2)
      walletv += (((walletv * ratio_de_mise) * list_play_main->p->p_odd->draw)-(walletv * ratio_de_mise));
    else walletv -= (walletv * ratio_de_mise);
  }
  wallet = walleth + walletd + walletv;
  printf("%f %f %f %f\n", walleth, walletd, walletv, wallet);

  sanctionate_every_neurons(ai, list_play_main);
  new_generation(ai, list_play_main);
  next_play(&list_play_main);

  i++;
}


  destruct_ai(ai);
  clean();

  return 0;
}